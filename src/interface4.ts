// 인터페이스에 정의된 속성으로 인한 타입추론 방식 테스트, 인터페이스에 추가 속성 허용 방법

export {};

interface Person {
  name?: string;
}

function doSomthingPerson(person: Person): void {
  console.log(person.name ?? '이름이 설정되어 있지 않은 Person');
}

doSomthingPerson({}); // 이름이 설정되어 있지 않은 Person
doSomthingPerson({ name: '홍길동' }); // 홍길동
// doSomthingPerson({ nama: '이순신' }); // 오류. 인터페이스의 name 속성과 이름이 일치하지 않기 때문에 오탈자로 감지는 되지 않음
// doSomthingPerson({ namea: '이순신' }); // 오류. 인터페이스의 name 속성과 이름이 일치하기 때문에 오탈자로 감지됨

// 만약 인터페이스의 속성 외에 다른 속성도 있는 경우 타입 추론을 무시하고 싶다면 형변환으로 무시할 수 있다.
function doSomthingPerson2(person: Person): void {
  console.log(person);
}
doSomthingPerson2({ age: 21 } as Person); // { age: 21 }

// 인터페이스를 정의할 때 추가 속성을 허용하고 싶다면 아래와 같이 정의하면 된다.
interface Person2 {
  name?: string;
  [propName: string]: any;
}

const person2: Person2 = {
  name: '김선생',
  age: 32,
  job: 'teacher',
};

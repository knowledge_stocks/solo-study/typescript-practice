export {};

// function이면서 객체 타입도 될 수 있는 인터페이스
interface Person {
  age: number;
  (job: string): void;
}

const person: Person = ((job: string) =>
  console.log(`${job} 일을 합니다.`)) as Person;
person.age = 21;

console.log(person.age);
person('선생님');

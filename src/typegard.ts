// 타입스크립트의 강력한 타입 추론 기능. 타입가드
export {};

interface Bird {
  fly(): void;
  layEggs(): void;
}

interface Fish {
  swim(): void;
  layEggs(): void;
}

interface Amphibia {
  swim(): void;
  layEggs(): void;
}

function getFishOrBird(): Fish | Bird | Amphibia {
  const rand = (Math.random() * 3) >> 0; // 0 이상 3 미만 랜덤
  if (rand === 0) {
    return {
      fly() {
        console.log('새~가 날아든다~');
      },
      layEggs() {
        console.log('새~가 알을낳는다~');
      },
    } as Bird;
  } else if (rand === 1) {
    return {
      swim() {
        console.log('물고기가 헤엄친다~');
      },
      layEggs() {
        console.log('물고기가 알을낳는다~');
      },
    } as Fish;
  } else {
    return {
      swim() {
        console.log('양서류가 헤엄친다~');
      },
      layEggs() {
        console.log('양서류가 알을낳는다~');
      },
    } as Amphibia;
  }
}

// 유니언타입을 타입가드 없이 그냥 호출하면 공통적인 부분만 호출할 수 있다.
let pet = getFishOrBird(); // pet은 Fish | Bird | Amphibia
pet.layEggs(); // Fish와 Bird, Amphibia의 공통부는 그냥 호출 가능
// pet.swim();    // 오류 pet이 Bird일수도 있기 때문
// pet.fly();    // 오류 pet이 Fish 혹은 Amphibia일수도 있기 때문

// in을 통한 타입가드
// pet은 Fish | Bird | Amphibia 이고
// 이중에서 swim을 가진 타입은 Fish와 Amphibia
if ('swim' in pet) {
  // 타입가드에 의해 이 블록 내부에서는 pet이 Fish | Amphibia로 인식된다.
  pet.swim();
}

// typeof를 통한 타입가드
function isString(x: any): x is string {
  return typeof x === 'string';
}

// 타입가드가 작동하지 않는 타입 체크 함수 예시
function worngIsString(x: any): boolean {
  return typeof x === 'string';
}

function initNumOrStr(): number | string {
  const rand = (Math.random() * 2) >> 0;
  if (rand === 0) {
    return 'Hello world';
  } else {
    return 1234;
  }
}

let numberOrString: number | string;
numberOrString = initNumOrStr();

// numberOrString.replace('world', 'earth'); // numberOrString은 number일수도 있기 때문에 에러

// isString의 반환형식은 'x is string'
if(isString(numberOrString)) {
  // isString으로 x가 string임을 확인했기 때문에
  // 타입가드에 의해서 이 블록 내부에서는 numberOrString가 string으로 인식된다.
  console.log(numberOrString.replace('world', 'earth'));
}

// worngIsString의 반환형식은 'boolean'
if (worngIsString(numberOrString)) {
  // worngIsString 내부에서 string 타입 체크를 했지만
  // 반환값이 일반 boolean이기 때문에 타입가드가 먹히지 않는다.
  // 따라서 아래 코드는 에러
  // console.log(numberOrString.replace('world', 'earth'));
}

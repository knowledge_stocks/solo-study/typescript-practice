// 인덱스 타입을 사용해서 프로퍼티 이름을 사용하는 부분에서 문자열 리터럴 타입으로 제한하기

export {};

interface Car {
  manufacturer: string;
  model: string;
  year: number;
}

let taxi: Car = {
  manufacturer: 'Toyota',
  model: 'Camry',
  year: 2014,
};

// 버그가 일어나기 좋은 plunk 함수
function buggyPluck(o: any, propertyNames: any[]): any[] {
  return propertyNames.map((n) => o[n]);
}

// 정상적인 프로퍼티 이름들을 넣었을 땐 정상 작동하지만
console.log(buggyPluck(taxi, ['manufacturer', 'model', 'year']));

// 비정상적인 프로퍼티 이름들을 넣었을 땐
// 입력한 프로퍼티 이름이 잘못된 것인지, 실제 값이 undefined인지 구분하기 어렵다.
console.log(buggyPluck(taxi, ['manufcturer', 'moel', 'month', 0]));

// 인덱스 타입을 사용해서 buggyPluck을 개선해서 잘못된 프로퍼티명 이름을 방지
// 풀어헤칠 타입 T와, T 안에 정의되어 있는 프로퍼티 이름들의 리터럴 유니온 타입인 P
// 반환값은 T안의 프로퍼티 P의 값을 배열로 반환하므로 T[P][]
function pluck<T, P extends keyof T>(o: T, propertyNames: P[]): T[P][] {
  return propertyNames.map((n) => o[n]);
}

// 정상적인 프로퍼티 이름들을 넣으면 당연히 정상 작동
console.log(pluck(taxi, ['manufacturer', 'model', 'year']));

// 비정상적인 프로퍼티 이름들을 넣었을 땐
// 그런 프로퍼티 이름들은 taxi의 타입인 Car에는 없기 때문에 에러
// console.log(pluck(taxi, ['manufcturer', 'moel', 'month', 0]));

// P를 따로 설정하지 않고 바로 keyof를 사용해도 된다.
function pluck2<T>(o: T, propertyNames: (keyof T)[]): T[keyof T][] {
  return propertyNames.map((n) => o[n]);
}
console.log(pluck2(taxi, ['manufacturer', 'model', 'year']));

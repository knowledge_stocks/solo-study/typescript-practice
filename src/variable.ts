let count = 0;
count += 1;
// count = '문자열로 바꾸려고하면 에러가 난다.';

const message: string = 'hello world';
console.log(message);
// message = null; // null 값도 설정할 수 없다.
let nullableMessage: string | null = 'string이거나 null';
nullableMessage = null;
// nullableMessage = undefined; // null이 가능해도 undefined는 허용하지 않음
let undefinedableMessage: string | undefined = 'string이거나 undefined';
undefinedableMessage = undefined;
// undefinedableMessage = null; // undefined가 가능해도 null은 허용하지 않음

const done: boolean = true;
let done2 = false; // 처음 선언할 때는 타입을 자동으로 유추해준다.
// done2 = 1; // 자동으로 타입이 유추된 경우에도 중간에 타입을 바꿀 수 없다.
// done2 = null; // null 값도 설정할 수 없다.

const numbers: number[] = [1, 2, 3];
const strings: string[] = ['hello', 'world'];

let color: 'red' | 'orange' | 'blue' = 'red'; // red, orange, blue만 할당 가능
color = 'blue';
// color = 'green'; // 에러

import TestModule, {
  BirdType,
  FishType,
  AmphibiaType,
  Animals,
} from './test-module-commonjs';

// TestModule에 점을 찍어보면 인텔리센스로 aaa랑 testfunction에 대한 type 정보가 출력된다.
console.log(TestModule);
console.log(TestModule.testfunction());

// d.ts에서 선언한 interface들도 참조할 수 있다.
const animal: BirdType | FishType | AmphibiaType = TestModule.getRandomAnimal();
animal.layEggs();
if ('swim' in animal) {
  animal.swim();
} else if ('fly' in animal) {
  animal.fly();
}
// TestModule.getRandomAnimal().workMethod(); // 에러로 실행 안 됨

// 요놈은 d.ts에서 declare 붙임
const someBird: BirdType = {
  fly() {
    console.log('난다~ 날어~');
  },
  layEggs() {
    console.log('낳는다~ 낳어~');
  },
};
someBird.fly();
someBird.layEggs();

// 요놈은 d.ts에서 declare 안 붙임
const someFish: FishType = {
  swim() {
    console.log('헤엄친다~');
  },
  layEggs() {
    console.log('낳는다~');
  },
};
someFish.swim();
someFish.layEggs();

console.log(Animals);

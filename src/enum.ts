// enum 선언, number 값과 비교

export {};

enum State {
  Resting, // 0
  Sitting, // 1
  Walking, // 2
  Running, // 3
  Flying, // 4
}

console.log(State.Resting); // 0
console.log(State.Flying); // 4

console.log();

enum State2 {
  Resting, //0
  Sitting = 3, // 3에서 하나씩 증가
  Walking, // 4
  Running, // 5
  Flying, // 6
}

console.log(State2.Resting); // 0
console.log(State2.Sitting); // 3
console.log(State2.Walking); // 4
console.log(State2.Flying); // 6

console.log(typeof State2.Sitting); // enum값의 타입이 number로 찍힌다.
console.log(State2.Sitting === 3); // number 타입으로 취급되어서 ===으로 비교해도 동일

function getSomeNumber(): 20 {
  return 20;
}

enum State3 {
  Resting, //0
  Sitting = getSomeNumber(), // 숫자 20밖에 반환을 못 하지만 그래도 함수라 그런가? 컴파일/실행 전에는 어떤 숫자라고 확정을 못 짓나보다.
  Walking = 2, // 앞에 enum값이 불확실하기 때문에 직접 값을 할당해주어야 한다.
  Running, // 3
  Flying, // 4
}

console.log();

enum State4 {
  Resting = 10,
  Sitting, // 11
  Walking = 2,
  Running, // 3
  Flying = 2, // 같은 값을 갖는 enum값을 여러개 설정 가능하다.
}

console.log(State4.Resting); // 10
console.log(State4.Sitting); // 11
console.log(State4.Walking); // 2
console.log(State4.Flying); // 2

console.log();

enum State5 {
  Resting = 'R', // string 값도 설정 가능하다.
  Sitting = 'S',
  Walking = 2,
  Running, // 3
  Flying, // 4
}

console.log(State5.Resting); // R
console.log(State5.Sitting); // S
console.log(State5.Walking); // 2
console.log(State5.Flying); // 4

console.log();

// 비트 결합형? enum
enum FileAccess {
  None = 0,
  Read = 1 << 0,
  Write = 1 << 1,
  ReadWrite = Read | Write,
}

// 열거형은 사실 object이지만 타입추론에서 이점이 있다.
console.log(typeof State);
console.log(State);

console.log();

// 예를들면 keyof와 조합될 때 일반적인 object와는 다르게 동작한다.
type StateStr = keyof typeof State; // State enum에 설정된 값을 문자열로 풀어서 문자열 유니언 열거형 타입으로 설정
// let stateStr: StateStr = 'WrongState'; // 에러 State에 설정되지 않은 값이기 때문에
let stateStr: StateStr = 'Flying';

// enum => string
console.log(State[State.Flying]);
console.log(typeof State[State.Flying]);

// string => enum
console.log(State5['Resting']);
console.log(State5['Flying']);

console.log();

// const Enum
const enum ConstState {
  Resting, // 0
  Sitting, // 1
  Walking, // 2
  Running, // 3
  Flying, // 4
  // Wrong = getSomeNumber(), // const Enum에서는 상수만 사용 가능하다.
}

// declare로 Ambient Enum 타입을 선언할 수 있다.
// Ambient Enum은 기존에 선언되어 있는 Enum을 묘사하기 위해서 사용된다.
// 그래서.. 이거 왜 쓰는건데?
declare enum FakeState2 {
  Resting, //0
  Sitting = 3, // 3에서 하나씩 증가
  Walking, // 4
  Running, // 5
  Flying, // 6
}

// console.log(FakeState2.Resting); // Ambient Enum은 직접 사용할 수는 없다. 단지 묘사만 할 뿐이다.

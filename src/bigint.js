// 요건 타입스크립트 쪽 학습은 아니지만, number를 사용할 때 주의해야할 것 같다.

console.log(
  `안전한 number 범위 ${Number.MIN_SAFE_INTEGER} ~ ${Number.MAX_SAFE_INTEGER}`
);

// Number.MAX_SAFE_INTEGER는 2^53-1 즉, 9007199254740991 와 같다.
console.log(`2^53-1: ${2 ** 53 - 1}`);

// number의 최대값보다 1 큰 수를 출력했을 때 오버플로우를 예상했는데
// 9007199254740992가 정상 출력된다.
console.log(`2^53(type): ${2 ** 53}(${typeof (2 ** 53)})`);

// 문제는 이 부분 최대값보다 2 큰수를 출력했을 때
// 9007199254740993이 아닌 9007199254740992가 출력된다.
console.log(`2^53+1(type): ${2 ** 53 + 1}(${typeof (2 ** 53 + 1)})`);

// 그리고 다시 최대값보다 3 큰수를 출력했을 때
// 9007199254740994가 정상 출력된다.
// 즉, Number.MAX_SAFE_INTEGER 보다 큰 수가 number 타입 변수로 사용될 경우
// 문제가 발생할때도 있고 제대로 나올때도 있고 이상하게 돌아간다.
console.log(`2^53+2(type): ${2 ** 53 + 2}(${typeof (2 ** 53 + 2)})`);

// 따라서 Number.MAX_SAFE_INTEGER 보다 큰 수를 다뤄야하는 경우에는 bigint 타입을 사용해야한다.
// number형 리터럴에 n을 붙이면 bigint형 리터럴이 된다.
console.log(`2n^53n+1n(type): ${2n ** 53n + 1n}(${typeof (2n ** 53n + 1n)})`);
console.log(`2n^53n+2n(type): ${2n ** 53n + 2n}(${typeof (2n ** 53n + 2n)})`);

// 매핑타입으로 기존 타입을 확장하는 방법

export {};

// 매핑타입으로 기존 타입의 모든 프로퍼티를 선택적 프로퍼티로 갖는 확장타입
type Partial<T> = {
  [P in keyof T]?: T[P];
};

// 매핑타입으로 기존 타입의 모든 프로퍼티를 readonly 프로퍼티로 갖는 확장타입
type Readonly<T> = {
  readonly [P in keyof T]: T[P];
};

// 일반적인 타입 설정
interface Car {
  manufacturer: string;
  model: string;
  year: number;
}

// 앞에서 매핑타입으로 만든 확장타입들로 Car 확장해보기
interface PartialCar extends Partial<Car> {}
interface ReadonlyCar extends Readonly<Car> {}

const partialCar: PartialCar = {}; // Car의 모든 프로퍼티들이 선택적
const readonlyCar: ReadonlyCar = {
  // Car의 모든 프로퍼티들이 읽기전용
  manufacturer: 'string',
  model: 'string',
  year: 2022,
};

// readonlyCar.manufacturer = 'asdf'; // 에러 읽기전용이기 때문에 변경 불가능

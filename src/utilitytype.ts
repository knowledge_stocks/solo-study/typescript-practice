// 타입스크립트에서 기본 제공되는 유틸리티 타입들

export {};

interface Address {
  email: string;
  address: string;
  phonenum: string;
}

// Partial: 멤버 속성들을 선택 속성으로 바꿔주는 타입
type PartialAddress = Partial<Address>; // Address의 내부 속성이 모두 선택 속성으로 된 새로운 타입
const partialAddr: PartialAddress = {}; // 모두 선택 속성이기 때문에 값을 설정하지 않아도 오류 없음

// Required: Partial과 반대로 멤버 속성들을 필수 속성으로 바꿔주는 타입
type RequiredAddress = Required<PartialAddress>;
// const requiredAddr: RequiredAddress = {}; // 모두 필수 속성이기 때문에 값을 설정하지 않아서 오류

// Pick: 특정 멤버 속성만 꺼내주는 타입
type EmailAndPhonenum = Pick<Address, 'email' | 'phonenum'>; // email이랑 phonenum 멤버만 뽑아서 새로운 타입 생성

// Omit: 특정 멤버 속성만 제거해주는 타입
type OnlyAddress = Omit<Address, 'email' | 'phonenum'>; // email이랑 phonenum 멤버 제거

// Readonly: 멤버 속성들을 읽기 전용으로 바꿔주는 타입
type ReadonlyAddress = Readonly<Address>;

// Record: 잘 안쓸거 같긴한데.. 동일한 멤버 속성들을 가진 레코드들을 생성해주는 타입
type RecordAddress = Record<'address1' | 'address2', PartialAddress>;
const record: RecordAddress = {
  address1: { email: 'email1' },
  address2: { email: 'email2' },
};

// Exclude: 유니온 타입에서 겹치는 타입 제거해준다.
interface ExcludeMembers {
  phonenum: string;
}

// Excluded1가 never가 나온다..?
// Address에서 ExcludeMembers 빼면 Address가 나와야할 것 같은데
// Address나 ExcludeMembers나 둘 다 object로 인식되나보다
type Excluded1 = Exclude<Address, ExcludeMembers>;

// 요렇게 해도 마찬가지로 never
type Excluded2 = Exclude<Address | ExcludeMembers, ExcludeMembers>;

// 요렇게 하면 string | number가 나온다.
// 인터페이스와 같은 타입으로는 안 되나보다.
type Excluded3 = Exclude<string | number | ExcludeMembers, ExcludeMembers>;

// Extract: 유니온 타입에서 겹치는 타입(교집합)을 찾아준다.
// Extract1은 결과물이 Address가 된다. 걍 인터페이스 끼리는 잘 안되는걸로
type Extract1 = Extract<Address, ExcludeMembers>;

// Extract2의 경우 number | "1" 이 결과로 나온다
type Extract2 = Extract<number | '0' | '1', '1' | '2' | number | boolean>

// 그런데 Extract3의 경우 number만 결과로 나온다.
// (string | number | '0' | '1')에서 string이 뒷부분의 '0'이랑 '1'을 무효화 시키는 것 같다.
type Extract3 = Extract<string | number | '0' | '1', '1' | '2' | number | boolean>

// 그 외.. 준내 많네..
// NonNullable: 유니온에서 null, undefined 제거
// Parameters: 함수 타입에서 매개변수의 타입들을 추출
// ConstructorParameters: 생성자에 들어가는 매개변수의 타입들을 추출
// ReturnType<T>: 함수의 반환 타입을 가져옴. 요거 redux의 액션 코드 짤때 유용함
// InstanceType: 요건 봐도 뭔지 몰겠다
// ThisParameterType: 함수의 this 매개변수의 타입을 가져온다
// OmitThisParameter: 함수에서 this 매개변수를 제거한다.
// ThisType: 요건 머꼬.. 걍 this 매개변수 명시적으로 쓰면 되는거 아닌가

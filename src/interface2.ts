interface Person {
  name: string;
  age?: number; // 물음표는 구현을 해도 되고 안해도 되는 값이라는 의미
}
interface Developer {
  name: string;
  age?: number;
  skills: string[];
  doWork(): void;
}

const person1: Person = {
  name: '김사람',
  age: 20,
};

const person2: Person = {
  name: '홍길동',
};

const expert: Developer = {
  name: '김개발',
  skills: ['javascript', 'react'],
  doWork: function () {
    console.log(`${this.name}은 일을 합니다.`);
  },
};

console.log(person1);
console.log(person2);
console.log(person1.age);
console.log(person2.age); // undefined
console.log(expert);
expert.doWork();

class Teacher implements Person {
  constructor(public name: string) {
    this.name = name;
  }
}

const teacherLee = new Teacher('이선생');
console.log(teacherLee);
// console.log(teacherLee.age); Teacher는 age를 구현하지 않아서 그런지 Person의 age에 접근할 수 없다.
console.log((teacherLee as Person).age); // Person으로 형변환을 한 뒤에는 age에 접근할 수 있는데, 당연하게도 undefined이다.

console.log((expert as Person).name); // 이게 왜 됨???
// expert는 Person과 관련 없는데 형변환이 된다..?

// 클래스는 instanceof로 확인이 가능한데
console.log(expert instanceof Teacher);

// 인터페이스는 instanceof로 확인이 안 된다.
// console.log(expert instanceof Person);
// console.log(expert instanceof Developer);

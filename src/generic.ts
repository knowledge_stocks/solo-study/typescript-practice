// 이렇게 만들어도 원하는 동작을 구현할 수 있지만,
// 반환값의 타입 체크를 못 하게 되고,
// 외부에서 이 함수를 호출할 때에도 함수 설명을 보고 어떤 어떤 형식이 사용되는지 알 수 없게 된다.
// function merge(a: any, b: any): any {
//   return {
//     ...a,
//     ...b,
//   };
// }

function merge<A, B>(a: A, b: B): A & B {
  return {
    ...a,
    ...b,
  };
}

const merged = merge({ foo: 1 }, { bar: 1 });

console.log(merged);

// 인터페이스에서 제네릭 사용 예시
interface Items<T> {
  list: T[];
}
const items: Items<string> = {
  list: ['a', 'b', 'c'],
};

// 타입에서 제네릭 사용 예시
type Items2<T> = {
  list: T[];
};
const items2: Items2<string> = {
  list: ['a', 'b', 'c'],
};

// 클래스에서 제네릭 사용 예시
class Queue<T> {
  list: T[] = [];
  get length(): number {
    return this.list.length;
  }
  enqueue(item: T) {
    this.list.push(item);
  }
  dequeue(): T | undefined {
    return this.list.shift();
  }
}
const queue = new Queue<number>();
queue.enqueue(1);
queue.enqueue(2);
console.log(queue.length);

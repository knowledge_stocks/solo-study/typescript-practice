// 이거 확장자랑, index 생략할라믄 뭐 설정해야 하는겨..
// 확장자 생략은 Babel이 해주는라고 한다.
import TestModule from './test-module/index.js';

console.log(TestModule);

// .d.ts가 없는 상태에서 getRandomAnimal의 반환 타입을 보면
// 아래와 같이 반환하는 오브젝트의 형태를 모두 유니온 해서 출력해준다.
/*
{
    fly(): void;
    layEggs(): void;
} | {
    swim(): void;
    layEggs(): void;
}
*/
// .d ts에 타입 정보를 추가하고 반환 타입을 다시 보면 아래와 같이 유니온 타입으로 깔끔하게 출력해준다.
// TestModule.BirdType | TestModule.FishType | TestModule.AmphibiaType
console.log(TestModule.getRandomAnimal());

// // d.ts에서 선언한 interface들도 참조할 수 있다.
// const animal: BirdType | FishType | AmphibiaType = TestModule.getRandomAnimal();
// animal.layEggs();
// if ('swim' in animal) {
//   animal.swim();
// } else if ('fly' in animal) {
//   animal.fly();
// }
// // TestModule.getRandomAnimal().workMethod(); // 에러로 실행 안 됨

// // 요놈은 d.ts에서 declare 붙임
// const someBird: BirdType = {
//   fly() {
//     console.log('난다~ 날어~');
//   },
//   layEggs() {
//     console.log('낳는다~ 낳어~');
//   },
// };
// someBird.fly();
// someBird.layEggs();

// // 요놈은 d.ts에서 declare 안 붙임
// const someFish: FishType = {
//   swim() {
//     console.log('헤엄친다~');
//   },
//   layEggs() {
//     console.log('낳는다~');
//   },
// };
// someFish.swim();
// someFish.layEggs();

// console.log(Animals);

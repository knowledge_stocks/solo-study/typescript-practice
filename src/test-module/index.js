export const Bird = {
  fly() {
    console.log('새~가 날아든다~');
  },
  layEggs() {
    console.log('새~가 알을낳는다~');
  },
};

export const Fish = {
  swim() {
    console.log('물고기가 헤엄친다~');
  },
  layEggs() {
    console.log('물고기가 알을낳는다~');
  },
};

export const Amphibia = {
  swim() {
    console.log('양서류가 헤엄친다~');
  },
  layEggs() {
    console.log('양서류가 알을낳는다~');
  },
};

function getRandomAnimal() {
  const rand = (Math.random() * 3) >> 0; // 0 이상 3 미만 랜덤
  if (rand === 0) {
    return {
      fly() {
        console.log('새~가 날아든다~');
      },
      layEggs() {
        console.log('새~가 알을낳는다~');
      },
    };
  } else if (rand === 1) {
    return {
      swim() {
        console.log('물고기가 헤엄친다~');
      },
      layEggs() {
        console.log('물고기가 알을낳는다~');
      },
    };
  } else {
    return {
      swim() {
        console.log('양서류가 헤엄친다~');
      },
      layEggs() {
        console.log('양서류가 알을낳는다~');
      },
    };
  }
}

export default {
  Animals: [Bird, Fish, Amphibia],
  getRandomAnimal,
};

import './test.js';

declare interface BirdType {
  fly(): void;
  layEggs(): void;
}

interface FishType {
  swim(): void;
  layEggs(): void;
}

interface AmphibiaType {
  swim(): void;
  layEggs(): void;
}

declare function testfunction(): string;
declare function aaa(str): string;
declare function getRandomAnimal(): BirdType | FishType | AmphibiaType;

// 오프젝트는 요런식으로 타입 설정
declare const Animals: [BirdType, FishType, AmphibiaType];

export {};

interface Shape {
  getArea(): number; // 추상 함수 getArea 추가
  // getWidth(): number { return 10; }; // 내부 구현이 있는 함수는 interface에 정의할 수 없다.
  // color: string; // 인터페이스에 멤버 변수를 선언해놓을 수 있다.
  // color2: string = ''; // 당연하지만 값은 할당할 수 없다.
}

// Shape를 구현상속한 Circle 정의
class Circle implements Shape {
  // 생성자 추가
  // 생성자의 인수에 접근자를 붙이면 해당 인수는 멤버 변수로 자동 설정된다.
  constructor(public radius: number) {
    this.radius = radius;
  }

  // Shape 인터페이스의 getArea 구현
  getArea() {
    return this.radius * this.radius * Math.PI;
  }
}

// Circle과 같은 방식으로 Rectangle도 Shape를 구현상속해서 정의
class Rectangle implements Shape {
  protected width: number;
  private height: number;

  constructor(width: number, height: number) {
    this.width = width;
    this.height = height;
  }

  getArea() {
    return this.width * this.height;
  }
}

const circle = new Circle(5);
const rect = new Rectangle(10, 5);

// 접근자를 생략한 Circle.radius는 바깥에서도 접근 가능 기본 public인가보다
console.log(circle.radius);

// protected, private로 선언한 멤버들은 외부에서 접근 불가능
// console.log(rect.width);
// console.log(rect.height);

// Shape를 상속한 Circle과 Rectangle을 배열로 선언
const shapes: Shape[] = [circle, rect];

shapes.forEach((shape) => {
  console.log(shape.getArea()); // 각 Shape가 정의한 getArea 호출
});

// Shape랑 같은 구조의 FakeShape 인터페이스 생성
interface FakeShape {
  getArea(): number;
}

console.log('FakeShape 테스트');

const fakeShapes: FakeShape[] = [circle, rect]; // circle, rect는 Shape를 구현상속했는데 오류 없이 넘어간다.

fakeShapes.forEach((shape) => {
  console.log(typeof shape);
  console.log(shape.getArea()); // FakeShape로도 정상 작동한다.
});
// TypeScript에서 인터페이스로 바로 생성된 인스턴스는 타입이 인터페이스의 형식가 아닌 object 형식으로 설정된다.
// 따라서 인터페이스의 구조만 같으면 같은 타입으로 인지하는 것 같다.

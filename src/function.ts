// function 함수명(인자, 인자형식): 반환형식
function sum(x: number, y: number): number | undefined {
  // return null; 반환 형식에 null이 없기 때문에 null은 불가
  if (x + y > 20) {
    return undefined; // number | undefined 이기 때문에 가능
  }
  return x + y;
}

// sum 함수를 사용하려고 입력하면 에디터가 아래와 같이 함수에 들어가는 타입 정보를 보여준다.
// sum(x: number, y: number): number | undefined
sum(1, 2);

// 인자 타입에 따른 오버라이딩은 일단 전역에서는 안 되는 걸로..
// function sum(numbers: number[]): number {
function sumArray(numbers: number[]): number {
  // reduce의 결과가 number라는 것을 자동으로 유추해준다.
  return numbers.reduce((acc, current) => acc + current, 0);
}

// 반환값이 없는 경우 void 사용
function returnNothing(): void {
  console.log('return nothing');
}

// 반환 형식을 undefined로 설정하면 명시적으로 undefined를 return 해주어야 한다.
function returnNothing2(): undefined {
  console.log('return nothing2');
  return undefined;
}

// 둘이 결과는 똑같이 undefined
console.log(returnNothing());
console.log(returnNothing2());
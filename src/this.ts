export {};

interface Person {
  name: string;
  age?: number;
}

interface Developer extends Person {
  skills: string[];
  doWork(): void;
  printThisDeveloper(this: Developer): void;
  printThisPerson(this: Person): void;
}

const developer1: Developer = {
  name: '박개발',
  age: 31,
  skills: ['react', 'typescript', 'html', 'css', 'js'],
  doWork: function (): void {
    console.log(`${this.name} ${this.age}세 일하는 중`);
  },
  printThisDeveloper: function (this: Developer) {
    console.log(`------- ThisDeveloper:`);
    console.log(this);
  },
  printThisPerson: function (this: Person) {
    console.log(`------- ThisPerson:`);
    console.log(this);
  },
};

developer1.doWork();
developer1.printThisDeveloper();
developer1.printThisPerson(); // 얘도 printThisDeveloper와 마찬가지로 developer1의 모든 데이터를 출력한다.

developer1.doWork = function (): void {
  console.log(this);
};

developer1.doWork();

function wrapFunction(func: () => void): () => void {
  return func;
}

wrapFunction(developer1.doWork)(); // 이 경우 this는 undefined
const doWork2 = developer1.doWork;
doWork2(); // 이 경우에도 this undefined

// this를 developer1로 설정하고 함수를 실행하는 예시들
doWork2.apply(developer1); // 얘는 args를 배열로 받고
doWork2.call(developer1); // 얘는 args를 ,단위로 받는 차이

// 함수에 this를 바인드하는 예시
console.log('---- bind this');
doWork2();
const bindedDoWork2 = doWork2.bind(developer1); // 얘는 doWork2의 this를 아예 바인드 해놓는다.
bindedDoWork2();
bindedDoWork2();

// 콜백에서 this의 타입을 제한하는 예시

interface Event {
  event: string;
}

interface Controller {
  addEventListener(onEvent: (this: void, e: Event) => void): void;
}

const controller: Controller = {
  addEventListener(onEvent: (this: void, e: Event) => void): void {
    const event: Event = {
      event: 'something'
    }
    onEvent(event);
  }
};

// this 타입 설정을 생략한 경우
class EventHandler {
  onEvent(e: Event) { // this 타입 생략
    console.log(`${e.event} 이벤트 발생!`);
    console.log(this);
  }
}
const handler = new EventHandler();
controller.addEventListener(handler.onEvent); // addEventListener에서 요구하는 this인 void로 정상 작동한다.

// this 타입 설정이 일치하지 않는 경우
class EventHandler2 {
  onEvent(this: EventHandler2, e: Event) { // this 타입 생략
    console.log(`${e.event} 이벤트 발생!`);
    console.log(this);
  }
}
const handler2 = new EventHandler2();
// controller.addEventListener(handler2.onEvent); // this의 타입이 맞지 않기 때문에 에러가 표기되고 실행되지 않는다.
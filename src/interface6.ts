// 타입스크립트에서는 인터페이스가 Class를 상속할 수 있다.
// 인터페이스가 클래스를 상속할 때 private, protected 멤버가 있는 경우와 public 멤버만 있는 경우의 차이

export {};

// private 멤버를 하나 가지고 있는 Control 클래스 선언
class Control {
  private state: any;
}

// Control 클래스를 상속하는 interface 선언
// 이 경우 Control의 private 멤버도 상속 받는다.
interface SelectableControl extends Control {
  select(): void;
}

// // private 멤버 때문에 인터페이스로 바로 객체를 생성하지 못하고 있다.
// // 방법이 있을 것 같은데.. 일단 이런 경우에는 클래스로 상속 후 사용해야하는 걸로 알아두자.
// let selectableControl: SelectableControl = {
//   state: 'state',
//   select(): void {
//     console.log('selected')
//   }
// }

// 클래스를 상속한 인터페이스는 이렇게 상속 받아야 하나보다.
class Button extends Control implements SelectableControl {
  select() {}
}

// SelectableControl이 Control에서 상속한 state를 명시적으로 선언해 줘봤는데
// 이 방식으로는 안 되는 듯 하다.
class Button2 implements SelectableControl {
  private state: any;
  select() {}
}

class TextBox extends Control {
  select() {}
}

// 타입스크립트 핸드북에서도 이 방식은 안되는 예시로 들고 있다.
// 안 되는게 맞는듯 하다.
class Image implements SelectableControl {
  private state: any;
  select() {}
}

// public이면 어떻게 되나 테스트
class Control2 {
  state: any;
}

interface SelectableControl2 extends Control2 {
  select(): void;
}

// 클래스에서 인터페이스로 상속한 멤버가 public이면 그냥 일반 인터페이스 구현상속하듯이 사용할 수 있다.
let selectableControl: SelectableControl2 = {
  state: 'state',
  select(): void {
    console.log('selected');
  },
};

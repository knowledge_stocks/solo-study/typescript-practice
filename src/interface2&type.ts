// 인터페이스 대신 type 사용해보기
type Person2 = {
  name: string;
  age?: number; // 물음표는 구현을 해도 되고 안해도 되는 값이라는 의미
};

// type은 이런식으로 합치는게 가능한가보다.
// 인터페이스의 extends랑 유사하다.
type Developer2 = Person2 & {
  skills: string[];
  doWork(): void;
};

const person3: Person2 = {
  name: '김사람',
  age: 20,
};

const person4: Person2 = {
  name: '홍길동',
};

const expert2: Developer2 = {
  name: '김개발',
  skills: ['javascript', 'react'],
  doWork: function () {
    console.log(`${this.name}은 일을 합니다.`);
  },
};

console.log(person3);
console.log(person4);
console.log(person3.age);
console.log(person4.age); // undefined
console.log(expert2);
expert2.doWork();

class Teacher2 implements Person2 {
  constructor(public name: string) {
    this.name = name;
  }
}

const teacherLee2 = new Teacher2('이선생');
console.log(teacherLee2);
// console.log(teacherLee.age); Teacher는 age를 구현하지 않아서 그런지 Person의 age에 접근할 수 없다.
console.log((teacherLee2 as Person2).age); // Person으로 형변환을 한 뒤에는 age에 접근할 수 있는데, 당연하게도 undefined이다.

console.log((expert2 as Person2).name); // 이게 왜 됨???
// expert는 Person과 관련 없는데 형변환이 된다..?

// 클래스는 instanceof로 확인이 가능한데
console.log(expert2 instanceof Teacher2);

// 인터페이스랑 type으로 선언한 타입은 instanceof로 확인이 안 된다.
// console.log(expert2 instanceof Person2);
// console.log(expert2 instanceof Developer2);

// 이와 같이 원하는 형식을 type으로 지정할 수 있다.
type People = Person2[];
const people: People = [person3, person4, expert2];

type Color = 'red' | 'blue' | 'green';
let color2: Color = 'red';
// let color3: Color = 'yellow'; // 당연히 요건 에러
